import React, { Component } from 'react'
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom'
import logo from './assets/img/logo.svg'
import './assets/scss/app.scss'
import Home from './components/home/home'

class App extends Component {
  render() {
    return (
      <div className="app">
        <Home />
      </div>
    )
  }
}

export default App
