import React, { Component } from 'react'
import AppHeader from '../header/appHeader'
import Banner from '../banner/banner'

class Home extends Component {
    state = {}
    render() {
        return (
            <>
                <AppHeader />
                <Banner />
            </>
        )
    }
}

export default Home